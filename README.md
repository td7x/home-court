# Home Court

This very simple module that identifies the path of the *most-parent* app or module - home court advantage for you module.

## Installation

``` bash
$ npm i -S home-court
```

## Usage

If you need the the path of the parent (or grand-parent) module simply import 'home-court' (the export is a string).

``` js
const APP_HOME = require('home-court');
console.log(APP_HOME);
```
