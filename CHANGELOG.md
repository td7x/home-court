# Change Log

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

<a name="0.6.1"></a>
## [0.6.1](https://gitlab.com/td7x/home-court/compare/v0.6.0...v0.6.1) (2017-07-14)


### Bug Fixes

* **cli:** relative to exec pwd ([1799b99](https://gitlab.com/td7x/home-court/commit/1799b99))



<a name="0.6.0"></a>
# [0.6.0](https://gitlab.com/td7x/home-court/compare/v0.5.1...v0.6.0) (2017-07-14)


### Features

* **core:** export sync fn ([003d18c](https://gitlab.com/td7x/home-court/commit/003d18c))



<a name="0.5.1"></a>
## [0.5.1](https://gitlab.com/td7x/home-court/compare/v0.5.0...v0.5.1) (2017-07-14)



<a name="0.5.0"></a>
# [0.5.0](https://gitlab.com/td7x/home-court/compare/v0.4.0...v0.5.0) (2017-07-12)


### Bug Fixes

* **doc:** typo in excluding spec ([3eafaef](https://gitlab.com/td7x/home-court/commit/3eafaef))


### Features

* **ci:** prettier fmt,standard lint, updated deps ([a4a0bc4](https://gitlab.com/td7x/home-court/commit/a4a0bc4))
* **core:** update deps, remove chai-as-promised ([2bd26ab](https://gitlab.com/td7x/home-court/commit/2bd26ab))
* **core:** update deps, remove chai-as-promised ([4ee42c6](https://gitlab.com/td7x/home-court/commit/4ee42c6))



<a name="0.4.0"></a>
# [0.4.0](https://gitlab.com/td7x/home-court/compare/v0.3.0...v0.4.0) (2017-06-19)


### Features

* **all:** new td7x conventions ([738d1c7](https://gitlab.com/td7x/home-court/commit/738d1c7))



<a name="0.3.0"></a>
# [0.3.0](https://gitlab.com/tom.davidson/home-court/compare/v0.2.3...v0.3.0) (2017-05-23)


### Bug Fixes

* **core:** clean up in prep for release ([b6faa89](https://gitlab.com/tom.davidson/home-court/commit/b6faa89))


### Features

* **ci:** enable standard version & release ([a8bc4dc](https://gitlab.com/tom.davidson/home-court/commit/a8bc4dc))
* **core:** cli invocation ([4365986](https://gitlab.com/tom.davidson/home-court/commit/4365986))



<a name="0.2.3"></a>
## [0.2.3](https://gitlab.com/tom.davidson/home-court/compare/v0.2.2...v0.2.3) (2017-05-07)


### Bug Fixes

* **core:** remove bin exec ([3c3aad4](https://gitlab.com/tom.davidson/home-court/commit/3c3aad4))



<a name="0.2.2"></a>
## [0.2.2](https://gitlab.com/tom.davidson/home-court/compare/v0.2.1...v0.2.2) (2017-05-07)


### Bug Fixes

* **docs:** update usage ([8fbfee0](https://gitlab.com/tom.davidson/home-court/commit/8fbfee0))



<a name="0.2.1"></a>
## [0.2.1](https://gitlab.com/tom.davidson/home-court/compare/v0.2.0...v0.2.1) (2017-05-07)


### Bug Fixes

* **core:** missing bin setting ([9345ff2](https://gitlab.com/tom.davidson/home-court/commit/9345ff2))



<a name="0.2.0"></a>
# 0.2.0 (2017-05-07)


### Features

* **core:** initial module ([d4b5418](https://gitlab.com/tom.davidson/home-court/commit/d4b5418))



<a name="0.1.0"></a>
# 0.1.0 (2017-05-07)


### Features

* **core:** initial module ([da80809](https://gitlab.com/td7x/home-court/commit/da80809))
