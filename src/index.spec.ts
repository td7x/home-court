import * as chai from "chai";
import "mocha";
import * as path from "path";
import * as home from ".";

const expect = chai.expect;

describe("module export", () => {
  const t = path.resolve(".");

  it("path should return home syncy", () => {
    expect(home.path).to.equal(t);
  });

  it("promise() should find home asyncy", async () => {
    const result = await home.promise();
    expect(result).to.equal(t);
  });
});
