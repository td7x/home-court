import * as findUp from "find-up";
import * as path from "path";

export function async(cwd: string = __dirname): Promise<string | undefined> {
  if (__dirname.includes("node_modules")) {
    return Promise.resolve(pathsFromOutside(cwd));
  } else {
    return findUp("package.json", { cwd }).then(
      wp => (wp ? path.dirname(wp) : undefined)
    );
  }
}

export function sync(cwd: string = __dirname): string {
  if (__dirname.includes("node_modules")) {
    return pathsFromOutside(cwd);
  } else {
    return path.dirname(findUp.sync("package.json", { cwd }));
  }
}

function pathsFromOutside(cwd: string = __dirname): string {
  const wp = cwd.split("/node_modules/", 1).pop();
  if (!wp) {
    throw new Error("UNDEFINED PATH");
  }
  return path.normalize(wp);
}
