import { async, sync } from "./home-court";
export const path = sync();
export { async as promise };
export { sync };
