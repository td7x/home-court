import * as chai from "chai";
import "mocha";
import * as path from "path";
import * as home from "./home-court";

const expect = chai.expect;

describe("home-court", () => {
  const t = path.resolve(".");

  it("async() should find home", async () => {
    const result = await home.async();
    expect(result).to.equal(t);
  });

  it("sync() should find home", () => {
    expect(home.sync()).to.equal(t);
  });
});
