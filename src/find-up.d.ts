// Type definitions for find-up
// Project: https://github.com/sindresorhus/find-up
// Definitions by: Tom Davidson <http://tomdavidson.org>

/*~ This is the module plugin template file. You should rename it to index.d.ts
 *~ and place it in a folder with the same name as the module.
 *~ For example, if you were writing a file for "super-greeter", this
 *~ file should be 'super-greeter/index.d.ts'
 */

/*~ Here, declare the same module as the one you imported above */
declare module "find-up" {
  interface FindUpOptions {
    /**
     * Directory to start from, defaults to 'process.cwd()'.
     *
     * @param cwd
     */
    cwd?: string;
  }

  interface FindUp {
    /**
     * Returns a promise for the found file path.
     *
     * @param filename
     */
    (filename: string, otps?: FindUpOptions): Promise<any>;

    /**
     * Returns the parsed JSON.
     *
     * @param filepath
     */
    sync(filenames: string, otps?: FindUpOptions): any;
  }

  const findUp: FindUp;

  export = findUp;
}
